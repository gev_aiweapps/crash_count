from datetime import datetime, timedelta
import requests

# App api token
app_token = "c7658d26dd430b713c8c77dfca5246d44598f14c"

owner_name = "gev-aiweapps"
app_name = "testapp"

base_url = "https://api.appcenter.ms"
errors_path = f"/v0.1/apps/{owner_name}/{app_name}/errors/errorCountsPerDay"

today = datetime.today().date()
yesterday = today - timedelta(days=1)

today_iso = today.isoformat()
yesterday_iso = yesterday.isoformat()


def get_errors_by_date(date_iso):
    response = requests.get(
        url=f"{base_url}{errors_path}",
        params={"start": date_iso, "end": date_iso, "errorType": "unhandledError"},
        headers={"X-API-Token": app_token, "accept": "application/json"}
    )
    return response.json()["count"]


# returns True if today errors count 10% more than yesterday and False otherwise
def estimate(today_errors, yesterday_errors):
    if not yesterday_errors and today_errors:
        return True
    elif not yesterday_errors:
        return False
    percentage = float(format((today_errors / yesterday_errors) * 100 - 100, ".2f"))
    print(percentage)
    return percentage > 10


today_errors_count = get_errors_by_date(today_iso)
yesterday_errors_count = get_errors_by_date(yesterday_iso)

print(f"today: {today_errors_count}, yesterday: {yesterday_errors_count}")
print(estimate(today_errors=today_errors_count, yesterday_errors=yesterday_errors_count))
